using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LossGameSound : MonoBehaviour
{
    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        TheCounterOfEnemies.defeat += Defeat;
    }

    private void Defeat(float numberOfEnemies)
    {
        
    }
}
