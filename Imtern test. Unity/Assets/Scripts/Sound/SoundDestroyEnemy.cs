using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDestroyEnemy : MonoBehaviour
{
    private AudioSource _soundDestroyEnemy;

    private void Start()
    {
        _soundDestroyEnemy = GetComponent<AudioSource>();
        DestroyEnemy.deathOfTheEnemy += DestroyEnemySound;
        DestroyAllEnemies.destroyEnemies += DestroyAllEnemiesSound;
    }

    private void DestroyAllEnemiesSound()
    {
        _soundDestroyEnemy.Play();
    }

    private void OnDestroy()
    {
        DestroyEnemy.deathOfTheEnemy -= DestroyEnemySound;
    }

    private void DestroyEnemySound()
    {
        _soundDestroyEnemy.Play();
    }
}
