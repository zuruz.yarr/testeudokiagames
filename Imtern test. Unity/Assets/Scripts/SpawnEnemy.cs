using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField]
    private List<LowEnemy> _enemyLow = new List<LowEnemy>();
    [SerializeField]
    private List<Transform> _spawnPoints = new List<Transform>();

    [SerializeField]
    private float _maxTimerSpawnEnemy;
    [SerializeField]
    private float _minTimerSpawnEnemy;
    private float _timer;

    private void Awake()
    {
        foreach (var enemy in _enemyLow)
        {
            enemy.DefaultStats();
        }
    }

    private void Start()
    {
        StartCoroutine(TimerSpawn());
    }


    IEnumerator TimerSpawn()
    {
        yield return new WaitForSeconds(_timer);
        Spawn();
        _timer = Random.Range(_minTimerSpawnEnemy, _maxTimerSpawnEnemy + 1);
        StartCoroutine(TimerSpawn());        
    }

    private void Spawn()
    {
        int randomEnemy = Random.Range(0, _enemyLow.Count);
        int randomSpawnPoint = Random.Range(0, _spawnPoints.Count);

        Instantiate(_enemyLow[randomEnemy], _spawnPoints[randomSpawnPoint]);
    }    

    public void BoostSpawnEnemies(float min, float max)
    {
        _maxTimerSpawnEnemy -= max;
        if (_maxTimerSpawnEnemy <= 0)
            _maxTimerSpawnEnemy = 0.1f;

        _minTimerSpawnEnemy -= min;
        if (_minTimerSpawnEnemy <= 0)
            _minTimerSpawnEnemy = 0.1f;
    }

    public List<LowEnemy> ListEnemiesLow()
    {
        return _enemyLow;
    }

    public void FrozenTheTimer(float time)
    {
        StopAllCoroutines();
        Invoke("StartTimerSpawnEnemy", time);
    }   
    private void StartTimerSpawnEnemy()
    {
        StartCoroutine(TimerSpawn());
    }
}
