using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject _mainMenuWindow;
    [SerializeField]
    private GameObject _listOfRecordsWindow;
    [SerializeField]
    private GameObject _titlesWindow;

    private void Start()
    {
        _mainMenuWindow.SetActive(true);
        _listOfRecordsWindow.SetActive(false);
        _titlesWindow.SetActive(false);
    }

    public void NewGameButton()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1;
    }

    public void ListOfRecordButton()
    {
        _mainMenuWindow.SetActive(false);
        _listOfRecordsWindow.SetActive(true);
    }

    public void TitlesButton()
    {
        _mainMenuWindow.SetActive(false);
        _titlesWindow.SetActive(true);
    }

    public void ExitGameButton()
    {
        
        Application.Quit();
    }

    public void BackToMenuButton()
    {
        _mainMenuWindow.SetActive(true);
        _listOfRecordsWindow.SetActive(false);
        _titlesWindow.SetActive(false);
    }

    
}
