using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseInTheHealthOfEnemies : MonoBehaviour
{
    [SerializeField]
    private SpawnEnemy _spawnEnemy;

    [SerializeField]
    private float _additionalHealth;

    [SerializeField, Tooltip("��������� ����� � ����� ����� ����������� �����")]
    private int _increaseThreshold;
    private int _deathEnemyCount;

    void Start()
    {
        DestroyEnemy.deathOfTheEnemy += IncreasedHealth;
    }
    private void OnDestroy()
    {
        DestroyEnemy.deathOfTheEnemy -= IncreasedHealth;
    }

    private void IncreasedHealth()
    {
        _deathEnemyCount++;
        if(_deathEnemyCount % _increaseThreshold == 0)
        {
            foreach (var enemy in ListOfEnemiesOnTheField.EnemiesOnTheField)
            {
                enemy.IncreaseInTheHealth(_additionalHealth);
            }

            foreach (var enemyLow in _spawnEnemy.ListEnemiesLow())
            {
                enemyLow.IncreaseInTheHealth(_additionalHealth);
            }
        }
    }    
}
