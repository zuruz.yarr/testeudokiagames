using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReducingSpawnTimeEnemies : MonoBehaviour
{
    [SerializeField]
    private SpawnEnemy _spawnEnemy;

    [SerializeField]
    private float _reducingTheMaxSpawnTime;
    [SerializeField]
    private float _reducingTheMinSpawnTime;

    [SerializeField, Tooltip("��������� ����� � ����� ����� ��������� ����� ������")]
    private int _loweringThreshold;
        
    private int _deathEnemyCount;

    private void Start()
    {
        DestroyEnemy.deathOfTheEnemy += ReducingSpawnTime;
    }

    private void ReducingSpawnTime()
    {
        _deathEnemyCount++;
        if(_deathEnemyCount % _loweringThreshold == 0)
        _spawnEnemy.BoostSpawnEnemies(_reducingTheMinSpawnTime, _reducingTheMaxSpawnTime);
    }
}
