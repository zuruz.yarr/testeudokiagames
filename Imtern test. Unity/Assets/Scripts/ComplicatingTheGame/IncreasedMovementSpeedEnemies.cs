using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreasedMovementSpeedEnemies : MonoBehaviour
{
    [SerializeField]
    private SpawnEnemy _spawnEnemy;

    [SerializeField]
    private float _additionalSpeed;

    [SerializeField, Tooltip("��������� ����� � ����� ����� ����������� �������� ������������")]
    private int _increaseThreshold;
    private int _deathEnemyCount;

    void Start()
    {
        DestroyEnemy.deathOfTheEnemy += IncreasedMovementSpeed;
    }
    private void OnDestroy()
    {
        DestroyEnemy.deathOfTheEnemy -= IncreasedMovementSpeed;
    }

    private void IncreasedMovementSpeed()
    {
        _deathEnemyCount++;
        if (_deathEnemyCount % _increaseThreshold == 0)
        {
            foreach (var enemy in ListOfEnemiesOnTheField.EnemiesOnTheField)
            {
                enemy.IncreasedMovementSpeed(_additionalSpeed);
            }

            foreach (var enemyLow in _spawnEnemy.ListEnemiesLow())
            {
                enemyLow.IncreasedMovementSpeed(_additionalSpeed);
            }
        }
    }
}
