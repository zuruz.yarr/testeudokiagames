using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour
{
    private BaseEnemy _baseEnemy;

    public delegate void DeathOfTheEnemy();
    public static event DeathOfTheEnemy deathOfTheEnemy;

    public delegate void DestroyEnemyParticleSystem(GameObject gameObject);
    public static event DestroyEnemyParticleSystem _destroyEnemyParticleSystem;

    [SerializeField]
    private float _oneTapDamage;
    private float _health;

    private void Awake()
    {
        _baseEnemy = GetComponent<BaseEnemy>();
    }

    private void Start()
    {
        DestroyAllEnemies.destroyEnemies += EventDestroyEnemies;
        _health = _baseEnemy.HealsEnemy();
    }
    private void OnDestroy()
    {
        DestroyAllEnemies.destroyEnemies -= EventDestroyEnemies;
        ListOfEnemiesOnTheField.EnemiesOnTheField.Remove(_baseEnemy);
    }

    private void OnMouseDown()
    {
        ObtainingDamage();
        HealthTesting();
    }

    private void ObtainingDamage()
    {
        _health -= _oneTapDamage;
    }

    private void HealthTesting()
    {
        if (_health <= 0)
        {
            deathOfTheEnemy();
            _destroyEnemyParticleSystem(this.gameObject);
            Destroy(gameObject, 0.5f);
        }
    }

    private void EventDestroyEnemies()
    {
        _destroyEnemyParticleSystem(this.gameObject);
        Destroy(gameObject, 0.5f);
    }

    public void DestroyEnemyDeafetGame()
    {
        Destroy(gameObject);
    }
}
