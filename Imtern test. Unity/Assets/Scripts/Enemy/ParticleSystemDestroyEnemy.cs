using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemDestroyEnemy : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _destroyEnemy;

    private void Start()
    {
        DestroyEnemy._destroyEnemyParticleSystem += DestroyEnemyParticleSystem;
    }
    private void OnDestroy()
    {
        DestroyEnemy._destroyEnemyParticleSystem -= DestroyEnemyParticleSystem;
    }

    private void DestroyEnemyParticleSystem(GameObject gameObject)
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<BaseEnemy>().FreezeSpeed();
        Instantiate(_destroyEnemy, gameObject.transform);
    }
}
