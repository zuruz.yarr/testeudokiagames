using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCreatingAnAnemy : MonoBehaviour
{
    public delegate void AddingAnAnimy();
    public static event AddingAnAnimy addingAnAnimy;


    void Start()
    {
        addingAnAnimy();
    }
}
