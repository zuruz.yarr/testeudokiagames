using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
    [SerializeField]
    private float _baseSpeed;
    [SerializeField]
    private float _maxSpeed;
    public float _speed;

    [SerializeField]
    private float _baseHeals;
    [SerializeField]
    private float _maxHealth;
    public float _health;
    
    public float HealsEnemy()
    {
        return _health;
    }

    public float SpeedEnemy()
    {
        return _speed;
    }

    public void IncreasedMovementSpeed(float additionalSpeed)
    {
        _speed += additionalSpeed;
        if (_speed >= _maxSpeed)
            _speed = _maxSpeed;
    }

    public void IncreaseInTheHealth(float additionalHealth)
    {
        _health += additionalHealth;
        if (_health >= _maxHealth)
            _health = _maxHealth;
    }

    public void DefaultStats()
    {
        _speed = _baseSpeed;
        _health = _baseHeals;
    }

    public void FreezeSpeed()
    {
        _speed = 0;
    }
}
