using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionOfMovement : MonoBehaviour
{
    [SerializeField]
    private float _minTimerRotation;
    [SerializeField]
    private float _maxTimerRotation;
    private float _timer;

    void Start()
    {
        _timer = RandomTimeChangingTheDirectionOfMovement();
    }

    void Update()
    {
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            RandomRotation();
            _timer = RandomTimeChangingTheDirectionOfMovement();
        }
    }

    private float RandomTimeChangingTheDirectionOfMovement()
    {
        return _timer = Random.Range(_minTimerRotation, _maxTimerRotation);
    }

    private void RandomRotation()
    {
        float r = Random.Range(-90, 91f);
        transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y + r, transform.rotation.z));
    }

    private void OnTriggerEnter(Collider other)
    {
        transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y + 180f, transform.rotation.z));
    }    
}
