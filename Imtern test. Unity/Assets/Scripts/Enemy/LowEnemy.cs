using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LowEnemy : BaseEnemy
{
    private Rigidbody _rb;


    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        ListOfEnemiesOnTheField.EnemiesOnTheField.Add(this);
    }

    private void OnDestroy()
    {
        ListOfEnemiesOnTheField.EnemiesOnTheField.Remove(this);
    }

    private void Update()
    {
        _rb.velocity = transform.forward * SpeedEnemy();
    }
}
