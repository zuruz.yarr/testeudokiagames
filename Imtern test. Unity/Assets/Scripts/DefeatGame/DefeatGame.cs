using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefeatGame : MonoBehaviour
{
    [SerializeField]
    private GameObject _theWindowOfLoss;
    [SerializeField]
    private float _TheNumberOfEnemiesOnTheFieldToLose;

    public delegate void GameOver();
    public static event GameOver gameOver;

    void Start()
    {
        _theWindowOfLoss.SetActive(false);
        TheCounterOfEnemies.defeat += Defeat;
        
    }
    private void OnDestroy()
    {
        TheCounterOfEnemies.defeat -= Defeat;
    }

    private void Defeat(float numberOfEnemies)
    {
        if(numberOfEnemies >= _TheNumberOfEnemiesOnTheFieldToLose)
        {            
            _theWindowOfLoss.SetActive(true);
            foreach (var enemy in ListOfEnemiesOnTheField.EnemiesOnTheField)
            {
                enemy.GetComponent<DestroyEnemy>().DestroyEnemyDeafetGame();
            }
            Time.timeScale = 0;

            gameOver();
        }
    }    
}
