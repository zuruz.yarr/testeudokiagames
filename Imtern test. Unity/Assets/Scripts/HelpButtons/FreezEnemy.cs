using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreezEnemy : MonoBehaviour
{

    [SerializeField]
    private GameObject _button;
    private Image _image;
    private bool _involved;

    void Start()
    {
        _involved = false;
        _image = _button.GetComponent<Image>();
    }

    public void FreezeEnemyButton()
    {
        if (!_involved)
        {
            foreach (var enemy in ListOfEnemiesOnTheField.EnemiesOnTheField)
            {
                enemy.GetComponent<BaseEnemy>().FreezeSpeed();
            }            
            _image.color = new Color(0.2f, 0.2f, 0.2f);
        }
        _involved = true;
    }
}
