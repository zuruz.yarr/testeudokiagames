using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyAllEnemies : MonoBehaviour
{
    public delegate void DestroyEnemies();
    public static event DestroyEnemies destroyEnemies;

    [SerializeField]
    private GameObject _button;
    private Image _image;
    private bool _involved;

    private void Start()
    {
        _involved = false;
        _image = _button.GetComponent<Image>();
    }

    public void DestroyAnemiesEvent()
    {
        if (!_involved)
        {
            destroyEnemies();
            _image.color = new Color(0.2f, 0.2f, 0.2f);
        }
        _involved = true;
    }
}
