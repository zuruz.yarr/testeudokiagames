using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrozenTheTimeOfSpawn : MonoBehaviour
{
    [SerializeField]
    private SpawnEnemy _spawnEnemy;

    [SerializeField]
    private GameObject _button;
    private Image _image;
    private bool _involved;

    [SerializeField]
    private float _timerFrozenOfSpawn;

    void Start()
    {
        _involved = false;
        _image = _button.GetComponent<Image>();
    }

    public void FrozenTheTime()
    {
        if(!_involved)
        {
            _spawnEnemy.FrozenTheTimer(_timerFrozenOfSpawn);
            _image.color = new Color(0.2f, 0.2f, 0.2f);
        }
        _involved = true;
    }
}
