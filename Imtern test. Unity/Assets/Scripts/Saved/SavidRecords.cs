using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavidRecords : MonoBehaviour
{
    [SerializeField]
    private ScoreCout _scoreCount;
    [SerializeField]
    private int _maxNumberOfRecords;

    private bool a;

    private void Start()
    {
        a = false;
        DefeatGame.gameOver += GameOver;
    }
    private void OnDestroy()
    {
        DefeatGame.gameOver -= GameOver;
    }

    private void GameOver()
    {
        for (int i = 0; i < ListRecords.listRecords.Count; i++)
        {
            if (ListRecords.listRecords[i] == _scoreCount.Score())
                break;
            else
                a = true;
        }

        if(a)
            ListRecords.listRecords.Add(_scoreCount.Score());


        SavidListRecords();
        ListRecords.listRecords.Sort();
        ListRecords.listRecords.Reverse();
    }

    private void SavidListRecords()
    {
        for (int i = 0; i < ListRecords.listRecords.Count; i++)
        {
            PlayerPrefs.SetFloat("Record" + i, ListRecords.listRecords[i]);
        }
    }
}
