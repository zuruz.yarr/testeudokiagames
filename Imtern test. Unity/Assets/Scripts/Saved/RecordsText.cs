using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordsText : MonoBehaviour
{
    [SerializeField]
    private int _maxNumberOfRecords;

    [SerializeField]
    private List<Text> _listTopRecordsText = new List<Text>();

    private void Start()
    {
        for (int i = 0; i < _maxNumberOfRecords; i++)
        {
            ListRecords.listRecords.Add(PlayerPrefs.GetFloat("Record" + i));
        }
    }

    public void TopRecords()
    {
        for (int i = 0; i < _listTopRecordsText.Count; i++)
        {
            _listTopRecordsText[i].text = ListRecords.listRecords[i].ToString();
        }
    }
    
    public void ClearRecordList()
    {
        ListRecords.listRecords.Clear();

        PlayerPrefs.DeleteAll();

        for (int i = 0; i < _maxNumberOfRecords; i++)
        {
            if (PlayerPrefs.GetFloat("Record" + i) > 0)
                ListRecords.listRecords.Add(PlayerPrefs.GetFloat("Record" + i));
            else
                ListRecords.listRecords.Add(0);
        }

        for (int i = 0; i < _listTopRecordsText.Count; i++)
        {
            _listTopRecordsText[i].text = ListRecords.listRecords[i].ToString();
        }
    }
}
