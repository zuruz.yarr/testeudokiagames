using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TheCounterOfEnemies : MonoBehaviour
{
    [SerializeField]
    private Text _theCounterOfEnimiesText;
    private static float _numberOfEnemies;

    public delegate void Defeat(float numberOfEnemies);
    public static event Defeat defeat;   

    void Start()
    {
        _numberOfEnemies = 0;
        EventCreatingAnAnemy.addingAnAnimy += AddingAnEnemyToTheField;
        DestroyEnemy.deathOfTheEnemy += RemovingAnEnemyOnTheFiled;
        DestroyAllEnemies.destroyEnemies += DestroyAllEnemiesEvent;
    }
    private void OnDestroy()
    {
        EventCreatingAnAnemy.addingAnAnimy -= AddingAnEnemyToTheField;
        DestroyEnemy.deathOfTheEnemy -= RemovingAnEnemyOnTheFiled;
    }
    
    private void AddingAnEnemyToTheField()
    {
        _numberOfEnemies++;
        _theCounterOfEnimiesText.text = _numberOfEnemies.ToString();
        defeat(_numberOfEnemies);
    }

    private void RemovingAnEnemyOnTheFiled()
    {
        _numberOfEnemies--;
        _theCounterOfEnimiesText.text = _numberOfEnemies.ToString();
    }

    public static float NumberOfEnemies()
    {
        return _numberOfEnemies;
    }

    private void DestroyAllEnemiesEvent()
    {
        _numberOfEnemies = 0;
        _theCounterOfEnimiesText.text = _numberOfEnemies.ToString();
    }
}
