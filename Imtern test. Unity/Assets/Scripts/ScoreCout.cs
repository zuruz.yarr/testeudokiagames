using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCout : MonoBehaviour
{
    [SerializeField]
    private Text _scoreCounterText;
    private float _scoreCounter;

    void Start()
    {
        DestroyEnemy.deathOfTheEnemy += CreditingAPoint; 
    }
    private void OnDestroy()
    {
        DestroyEnemy.deathOfTheEnemy -= CreditingAPoint;
    }

    private void CreditingAPoint()
    {
        _scoreCounter++;
        _scoreCounterText.text = _scoreCounter.ToString();
    }

    public float Score()
    {
        return _scoreCounter;

    }
}
